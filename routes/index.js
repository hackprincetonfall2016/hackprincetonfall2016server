var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'New View' });
});

router.get('/about', function(req, res, next) {
  res.render('about', { title: 'About' });
});

router.get('/matchme', function(req, res, next) {
  res.render('matchme', { title: 'matchme' });
});

router.get('/messaging', function(req, res, next) {
  res.render('messaging', { title: 'matchme' });
})

router.get('/acceptreject', function(req, res, next) {
  res.render('acceptreject', { title: 'acceptreject' });
})

router.get('/humans.txt', function(req, res, next) {
  res.render('humans', { title: 'humans' });
})
module.exports = router;
