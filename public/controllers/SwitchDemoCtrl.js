app.controller('SwitchDemoCtrl', function($scope) {
 $scope.data = {
    cb1: true,
    message: "Republican Party"
  };

  $scope.changed = function(cbState) {
  	if($scope.data.cb1===true){
  	$scope.data.message = "Democrat Party";
  }
  else{
  	$scope.data.message= "Republican Party";
  }
  };
});